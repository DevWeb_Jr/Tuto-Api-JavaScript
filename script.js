const APIKEY = '62c28e834631c3fec8ba0451d3573532';

/* Appel à l' API OpenWeather en passant un ville en parametre*/
let apiCall = function (city) {
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${APIKEY}&units=metric&lang=fr`;

    fetch(url).then(response =>
        response.json().then(data => {
            console.log(data);
            document.querySelector('#city').innerHTML =
                "<i class=\"fas fa-city text-center\"></i>" + data.name;
            document.querySelector('#temp').innerHTML =
                "<i class=\"fas fa-temperature-high text-center\"></i>" + data.main.temp + '°';
            document.querySelector('#humidity').innerHTML =
                "<i class=\"fas fa-tint text-center\"></i>" + data.main.humidity + '%';
            document.querySelector('#wind').innerHTML =
                "<i class=\"fas fa-wind text-center\"><i/></br>" + data.wind.speed + 'km/h';
        })
    ).catch(err => console.log('Erreur : ' + err));

}

/* écoute de l'évènement lors de la soumission du formulaire*/
document.querySelector('form').addEventListener('submit', function(e){
    e.preventDefault();
    let ville = document.querySelector('#input-city').value;

    apiCall(ville);
});

/*par défaut*/
apiCall('Medine');